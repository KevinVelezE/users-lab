import sys

from psycopg2 import pool

from logger_base import log


class Connection:
    """
    Clase para la conexión a la base de datos.
    """

    _DATABASE = "test_db"
    _USER = "postgres"
    _PASSWORD = "postgres"
    _HOST = "localhost"
    _PORT = "5432"
    _MIN_CONNECTIONS = 1
    _MAX_CONNECTIONS = 10
    _pool = None

    @classmethod
    def get_pool(cls):
        """
        Retorna el pool de conexiones.
        """
        if cls._pool is None:
            try:
                cls._pool = pool.SimpleConnectionPool(
                    cls._MIN_CONNECTIONS,
                    cls._MAX_CONNECTIONS,
                    database=cls._DATABASE,
                    user=cls._USER,
                    password=cls._PASSWORD,
                    host=cls._HOST,
                    port=cls._PORT,
                )
                log.debug(f"Pool de conexiones creado: {cls._pool}")
                return cls._pool
            except Exception as e:
                log.error(f"Error al obtener el pool de conexiones: {e}")
                sys.exit(1)
        else:
            return cls._pool

    @classmethod
    def get_connection(cls):
        """
        Retorna una conexión.
        """
        try:
            conection = cls.get_pool().getconn()
            log.debug(f"Conexión del pool obtenida: {conection}")
            return conection
        except Exception as e:
            log.error(f"Error al obtener una conexión: {e}")
            sys.exit()

    @classmethod
    def close_connection(cls, conection):
        """
        Cierra una conexión.
        """
        try:
            cls.get_pool().putconn(conection)
            log.debug(f"Conexión del pool cerrada: {conection}")
        except Exception as e:
            log.error(f"Error al cerrar una conexión: {e}")
            sys.exit()

    @classmethod
    def close_all_connections(cls):
        """
        Cierra todas las conexiones.
        """
        try:
            cls.get_pool().closeall()
            log.debug("Todas las conexiones cerradas")
        except Exception as e:
            log.error(f"Error al cerrar todas las conexiones: {e}")
            sys.exit()
