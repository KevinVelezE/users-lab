from conection import Connection
from logger_base import log


class PoolCursor:
    def __init__(self):
        self.connection = None
        self.cursor = None

    def __enter__(self):
        """
        Connect to the database and create a cursor
        """
        log.debug("Inicio del metodo with __enter__")
        self.connection = Connection.get_connection()
        self.cursor = self.connection.cursor()
        return self.cursor

    def __exit__(self, exc_type, exc_value, traceback):
        """
        Close the cursor and connection
        """
        log.debug("se ejecuta del metodo with __exit__")
        if exc_value:
            self.connection.rollback()
            log.error(f"Error en la transacción: {exc_value} {exc_type} {traceback}")
        else:
            self.connection.commit()
            log.debug("Transacción exitosa")
