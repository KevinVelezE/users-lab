from logger_base import log
from user import User
from user_dao import UserDAO

opcion = None
while opcion != 5:
    print(
        """
    1. Listar usuarios
    2. Insertar usuario
    3. Actualizar usuario
    4. Eliminar usuario
    5. Salir
    """
    )
    opcion = int(input("Ingrese una opcion: "))
    if opcion == 1:
        usuarios = UserDAO.SELECT()
        for usuario in usuarios:
            log.info(usuario)
    elif opcion == 2:
        username = input("Ingrese el username: ")
        password = input("Ingrese el password: ")
        user = User(username=username, password=password)
        user_inserted = UserDAO.INSERT(user)
        log.info(f"Usuarios insertados: {user}")
    elif opcion == 3:
        user_id = int(input("Ingrese el id del usuario: "))
        username = input("Ingrese el username: ")
        password = input("Ingrese el password: ")
        user = User(user_id=user_id, username=username, password=password)
        user_updated = UserDAO.UPDATE(user)
        log.info(f"Usuarios actualizados: {user}")
    elif opcion == 4:
        user_id = int(input("Ingrese el id del usuario: "))
        user = User(user_id=user_id)
        user_deleted = UserDAO.DELETE(user)
        log.info(f"Usuarios eliminados: {user_deleted}")
else:
    log.info("Saliendo...")
