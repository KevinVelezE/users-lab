from logger_base import log
from pool_cursor import PoolCursor
from user import User


class UserDAO:
    """
    DAO - Data Access Object
    CRUD - Create, Read, Update, Delete
    """

    _SELECT = "SELECT * FROM users ORDER BY user_id"
    _INSERT = "INSERT INTO users (username, password) VALUES (%s, %s)"
    _UPDATE = "UPDATE users SET username = %s, password = %s WHERE user_id = %s"
    _DELETE = "DELETE FROM users WHERE user_id = %s"

    @classmethod
    def SELECT(cls):
        with PoolCursor() as cursor:
            log.debug("seleccionando usuarios")
            cursor.execute(cls._SELECT)
            registers = cursor.fetchall()
            users = []
            for register in registers:
                user = User(register[0], register[1], register[2])
                users.append(user)
            return users

    @classmethod
    def INSERT(cls, user: User):
        with PoolCursor() as cursor:
            log.debug(f"insertando usuario: {user}")
            valores = (user.username, user.password)
            cursor.execute(cls._INSERT, valores)
            return cursor.rowcount

    @classmethod
    def UPDATE(cls, user: User):
        with PoolCursor() as cursor:
            log.debug(f"actualizando usuario: {user}")
            valores = (user.username, user.password, user.user_id)
            cursor.execute(cls._UPDATE, valores)
            return cursor.rowcount

    @classmethod
    def DELETE(cls, user: User):
        with PoolCursor() as cursor:
            log.debug(f"eliminando usuario: {user.user_id}")
            valores = (user.user_id,)
            cursor.execute(cls._DELETE, valores)
            return cursor.rowcount
